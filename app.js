const axios = require('axios');

axios.post('https://id.twitch.tv/oauth2/token?client_id={client_ID}&client_secret={client_secret}&grant_type=client_credentials')
  .then(function (response) {
    // handle success
    console.log(response.data);
    axios.get(`https://api.twitch.tv/helix/streams?game_id=33214`, {headers: {"Authorization": `Bearer ${response.data.access_token}`, "Client-ID": 'qw8y1vcdtuxe78bafqfg36k90emn1q'}})
    .then((odpowiedz) => {
        console.log(odpowiedz.data.data);
    }).catch((err) => {
        console.log(err);
    })
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .finally(function () {
    // always executed
  });